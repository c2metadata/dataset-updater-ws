FROM tomcat:9.0.19-jre8-alpine

LABEL maintainer="C2Metadata"

COPY ./dataset-updater-ws/target/dataset-updater-ws.war /tmp
#COPY dataset-updater-ws.war /tmp

RUN mkdir /usr/local/tomcat/webapps/dataset-updater-ws \
    && unzip /tmp/dataset-updater-ws.war -d /usr/local/tomcat/webapps/dataset-updater-ws
﻿* Encoding: UTF-8.
CD 'D:\Users\altergc\Documents\ICPSR\Project development\metadata_capture\scripts\GSS_36797\MVP_scripts\GSS_SPSS'.

get FILE='36797-0001-Data-C2M.sav'.

compute age_rec=age.
RECODE age_rec (15 thru 29.999=15) (30 thru 49.999=30) (50 thru Highest=50).

VARIABLE LABELS age_rec "Age recoded by RECODE".
value labels age_rec 15 "15-29" 30 "30-49" 50 "50+" .

RENAME VARIABLES (age_rec = age_3cat).

***********  COMPUTE **************************.
compute age_comp = 10*trunc(age/10).
VARIABLE LABELS age_comp "Age categorized by COMPUTE".

*********** tables *************************.
FREQUENCIES age_3cat age_comp.

EXECUTE.

********** IF *****************************.
IF age gt 70  age_comp = 70.

FREQUENCIES age_comp.

EXECUTE.

******** SELECT  CASES ***********************.
SELECT IF age GE 20.
select if age < 50.

FREQUENCIES age_comp.

EXECUTE.

********* SELECT VARIABLES ****************.
DELETE VARIABLES abany to absingle.
delete variables divlaw divlawy.

EXECUTE.

******* DEFINE MISSING VALUES ******************.
FREQUENCIES PARTYID1 TO PARTYID3.
MISSING VALUES PARTYID1 TO PARTYID3 (0,4 THRU 9).

FREQUENCIES PARTYID1 TO PARTYID3.

************  SORT VARIABLES  *****************.
**** NOTE -- SPSS is not case sensitive  *******.
COMPUTE XX1=1.
COMPUTE Xx2=2.
COMPUTE xx3=3.
COMPUTE xX4=4.
COMPUTE XX5=5.

SORT VARIABLES by NAME.

***** drop XX1 XX5 Xx2 *********************.
DELETE VARIABLES Xx2 to XX5.

******* AGGREGATE **********************************.
AGGREGATE
       /OUTFILE=* MODE=ADDVARIABLES
       /BREAK= cohort year
      /cy_inc_mean = mean(income)
      /cy_inc_n = nu(income)  .
 
execute.

FREQUENCIES cy_inc_n.
MEANS
   /tables = cy_inc_mean
   /tables = cy_inc_n.

EXECUTE.
 
******** FLOW CONTROL -- LOOP BY VARIABLE  *****************.
COMPUTE democrats=0.
COMPUTE republicans=0.
DO REPEAT partyidk= PARTYID1 PARTYID2 PARTYID3.
      IF (partyidk EQ 1) democrats=democrats +1 .
      IF (partyidk EQ 2) republicans=republicans +1. 
   END REPEAT.

CROSSTABS
   /tables = democrats BY republicans.

EXECUTE.

******* FLOW CONTROL -- LOOP BY NUMBER ******************.
compute idealfam=-1.
missing value idealfam (-1).
LOOP #k= 0 to 9.
  if (#k = chldidel) idealfam= #k.
  END LOOP.

CROSSTABS 
  /tables= chldidel by idealfam.

EXECUTE.



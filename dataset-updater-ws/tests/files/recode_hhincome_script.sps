﻿* Encoding: UTF-8.

********  open data file *******************.
GET  FILE='D:\Users\altergc\Documents\ICPSR\Project development\metadata_capture\scripts\Recode_v1\Recode_v1_before.sav'. 


******** copy income variable into HHinc_cat *************** .
COMPUTE HHinc_cat=HHincome. 

*******  recode HHinc_cat ***************************.
RECODE HHinc_cat (1 thru 24999=1) (25000 thru 49999=2) (50000 thru 74999=3) (75000 thru 99999=4) 
    (100000 thru 149999=5) (150000 thru Highest=6). 

******** add variable label *********************.
VARIABLE LABELS  HHinc_cat 'Household income grouped'. 

******** add value labels  ********************.
VALUE LABELS HHinc_cat 
  1.00 'Less than $25,000' 
  2.00 '$25,000-$50,000' 
  3.00 '$50,000-$75,000' 
  4.00 '$75,000-$100,000' 
  5.00 '$100,000-$150,000' 
  6.00 'Greater than $150,000'. 

******* table of frequencies *****************.
FREQUENCIES VARIABLES=HHinc_cat .

EXECUTE.




SAVE OUTFILE='D:\Users\altergc\Documents\ICPSR\Project development\metadata_capture\scripts\Recode_v1\Recode_v1_after.sav' .

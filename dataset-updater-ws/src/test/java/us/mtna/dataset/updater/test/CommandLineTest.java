package us.mtna.dataset.updater.test;

import java.io.File;

import org.c2metadata.sdtl.pojo.Program;
import org.junit.Ignore;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import us.mtna.dataset.updater.manager.UpdaterCLI;

public class CommandLineTest {

	public String xmlPath = "src/test/resources/filePairs/hhincome/hhincomeInputDdi.xml";
	public String sdtlPath = "src/test/resources/filePairs/hhincome/hhincomeSdtl.json";
	public String invalidSdtl = "src/test/resources/sdtl/sdtl (62).json";
	public String appendSdtl = "src/test/resources/sdtl/append.json";
	public String mergeXml1 = "src/test/resources/ddi/621ddi.xml";
	public String mergeXml2 = "src/test/resources/ddi/626ddi.xml";

	public String postman1 = "src/test/resources/postmanMultifileUseCase/postmanFiles_multiFile_fileMatchingUntrimmedA1.xml";
	public String postman2 = "src/test/resources/postmanMultifileUseCase/postmanFiles_multiFile_fileMatchingUntrimmedB1.xml";
	public String postmanSdtl = "src/test/resources/postmanMultifileUseCase/mergethissdtl.json";

	private ObjectMapper mapper = new ObjectMapper();
	public String fileSystemXml = "/Users/carsonhuntermtna/Postman/files/hhincome/hhincomeInputDdi.xml";
	public String fileSystemSdtl = "/Users/carsonhuntermtna/Postman/files/hhincome/sdtl.json";

	@Ignore
	@Test
	public void testFiles() throws Exception {
		File file = new File(xmlPath);
		File sdtl = new File(sdtlPath);

		System.out.println(file.getName());
		Program program = mapper.readValue(sdtl, Program.class);
		System.out.println(program.getCommandCount());
	}
	@Ignore
	@Test
	public void testCliOnFileSystem() throws Exception {
		String[] commandLineArgs = { "-xml", fileSystemXml, "-sdtl", fileSystemSdtl };
		UpdaterCLI.main(commandLineArgs);
	}
	@Ignore
	@Test
	public void testCliOnFileSystemWithOutputFile() throws Exception {
		String[] commandLineArgs = { "-xml", fileSystemXml, "-sdtl", fileSystemSdtl, "-output", "test_file.xml" };
		UpdaterCLI.main(commandLineArgs);
	}
	@Ignore
	@Test // text multiple files
	public void testMultiXml() throws Exception {
		String[] commandLineArgs = { "-xml", postman1, postman2, "-sdtl", postmanSdtl };
		System.out.println(commandLineArgs);
		UpdaterCLI.main(commandLineArgs);
	}
	@Ignore
	@Test // text multiple files
	public void testMultiXmlPostman() throws Exception {
		String[] commandLineArgs = { "-xml", mergeXml1, mergeXml2, "-sdtl", appendSdtl, "-output",
				"multi_test_file.xml" };
		System.out.println(commandLineArgs);
		UpdaterCLI.main(commandLineArgs);
	}
	@Ignore
	@Test
	public void testValiation() throws Exception {
		String[] commandLineArgs = { "-sdtl", sdtlPath, "-validate" };
		UpdaterCLI.main(commandLineArgs);
	}
	@Ignore
	@Test
	public void validateAppendSdtl() throws Exception {
		String[] commandLineArgs = { "-sdtl", appendSdtl, "-validate" };
		UpdaterCLI.main(commandLineArgs);
	}
	@Ignore
	@Test
	public void testValiationWithInvalidSdtl() throws Exception {
		String[] commandLineArgs = { "-sdtl", invalidSdtl, "-validate" };
		UpdaterCLI.main(commandLineArgs);
	}
	@Ignore
	@Test
	public void testHelpLong() throws Exception {
		String[] commandLineArgs = { "-help" };
		UpdaterCLI.main(commandLineArgs);
	}
	@Ignore
	@Test
	public void testVersionShort() throws Exception {
		String[] commandLineArgs = { "-version" };
		UpdaterCLI.main(commandLineArgs);
	}

}

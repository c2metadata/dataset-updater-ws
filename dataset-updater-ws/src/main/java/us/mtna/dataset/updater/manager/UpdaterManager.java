/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.dataset.updater.manager;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xmlbeans.XmlObject;
import org.c2metadata.sdtl.pojo.Program;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import us.mtna.data.transform.wrapper.sdtl.ValidationResult;
import us.mtna.dataset.updater.DatasetManagerUtils;
import us.mtna.dataset.updater.Ddi25OutputGenerator;
import us.mtna.dataset.updater.InputInformation;
import us.mtna.dataset.updater.ValidationUtility;
import us.mtna.dataset.updater.exception.UpdaterException;
import us.mtna.dataset.updater.impl.PseudocodeWebServiceImpl;
import us.mtna.pojo.DataSet;
import us.mtna.reader.Reader;
import us.mtna.reader.exceptions.ReaderException;
import us.mtna.reader.exceptions.RequestException;

public class UpdaterManager {

	private String pseudocodeHost;
	private String pseudocodePath;
	private String updaterVersion;
	private Log log = LogFactory.getLog(this.getClass());


	public UpdaterManager(String pseudocodeHost, String pseudocodePath, String updaterVersion) {
		this.pseudocodeHost = pseudocodeHost;
		this.pseudocodePath = pseudocodePath;
		this.updaterVersion = updaterVersion;
	}

	// TODO this needs to be refactored since it repeats 99% of the code in the
	// method below.
	public String uploadInuptInformation(InputInformation[] fileInformation, File programFile,
			XmlObject... xmlObjects) {

		List<DataSet> datasetsList = new ArrayList<>();
		Ddi25OutputGenerator generator = new Ddi25OutputGenerator();
		generator.setUpdaterVersion(updaterVersion);
		PseudocodeWebServiceImpl pseuodocode = new PseudocodeWebServiceImpl(pseudocodeHost, pseudocodePath);
		generator.setPseudocodeService(pseuodocode);
		ObjectMapper mapper = new ObjectMapper();
		Program program;
		try {
			program = mapper.readValue(programFile, Program.class);
		} catch (Exception e) {
			throw new UpdaterException("Could not parse the sdtl file", e);
		}

		if (fileInformation == null || fileInformation.length == 0) {
			return updateSingleFile(generator, program, Arrays.asList(xmlObjects));
		}

		// check that the file information matches up with the number of
		// datasets found.
		if (fileInformation.length != xmlObjects.length) {
			throw new RequestException("Found [" + fileInformation.length + "] InputInformation objects and ["
					+ xmlObjects.length + "] files. Requests must have exactly one input information per file");
		}

		int xmlIndex = 0;
		int primaries = 0;
		int primaryXmlIndex = 0;

		// assume all fields could be null
		for (InputInformation input : fileInformation) {
			XmlObject obj = xmlObjects[xmlIndex];

			DataSet dataset = readDataSets(obj, input.getXmlName(), input.getPosition());

			if (dataset == null) {
				throw new ReaderException("could not read datasets");
			}
			dataset.setScriptName(getScriptNameOrEquivalent(input, dataset));

			if (input.isPrimary()) {
				// move it to the front of the list
				// TODO does this need to happen at the end of the loop
				primaries++;
				datasetsList.add(0, dataset);
				primaryXmlIndex = xmlIndex;
			} else {
				datasetsList.add(dataset);
			}
			xmlIndex++;
		}
		if (primaries > 1) {
			// one or zero primaries is fine.
			throw new RequestException(
					"Multiple datasets were marked as primary, only one dataset may be marked as primary.");
		}
		// TODO do you need to merge the xml documents?
		XmlObject xmlObject = xmlObjects[primaryXmlIndex];

		return generator.getUpdatedXmlAsString(fileInformation, program, datasetsList, xmlObject);
	}

	/**
	 * Upload and organize all resources so the datasets can be read, updated,
	 * and returned as updated xml
	 * 
	 * @param fileInformation
	 * @param program
	 * @param xmlFiles
	 * @return
	 */
	public String uploadInuptInformation(InputInformation[] fileInformation, Program program,
			MultipartFile... xmlFiles) {
		List<DataSet> datasetsList = new ArrayList<>();
		Ddi25OutputGenerator generator = new Ddi25OutputGenerator();
		generator.setUpdaterVersion(updaterVersion);
		PseudocodeWebServiceImpl pseuodocode = new PseudocodeWebServiceImpl(pseudocodeHost, pseudocodePath);
		generator.setPseudocodeService(pseuodocode);

		List<XmlObject> xmlObjects = new LinkedList<>();
		for (MultipartFile file : xmlFiles) {
			XmlObject obj = DatasetManagerUtils.parseInputStream(file);
			xmlObjects.add(obj);
		}

		if (fileInformation == null || fileInformation.length == 0) {
			return updateSingleFile(generator, program, xmlObjects);
		}

		// check that the file information matches up with the number of
		// datasets found.
		if (fileInformation.length != xmlFiles.length) {
			throw new RequestException("Found [" + fileInformation.length + "] InputInformation objects and ["
					+ xmlFiles.length + "] files. Requests must have exactly one input information per file");
		}

		int xmlIndex = 0;
		int primaries = 0;
		int primaryXmlIndex = 0;

		// assume all fields could be null
		for (InputInformation input : fileInformation) {
			XmlObject obj = DatasetManagerUtils.parseInputStream(xmlFiles[xmlIndex]);
			DataSet dataset = readDataSets(obj, input.getXmlName(), input.getPosition());

			if (dataset == null) {
				throw new ReaderException("could not read datasets");
			}
			dataset.setScriptName(getScriptNameOrEquivalent(input, dataset));

			if (input.isPrimary()) {
				// move it to the front of the list
				// TODO does this need to happen at the end of the loop
				primaries++;
				datasetsList.add(0, dataset);
				primaryXmlIndex = xmlIndex;
			} else {
				datasetsList.add(dataset);
			}
			xmlIndex++;
		}
		if (primaries > 1) {
			// one or zero primaries is fine.
			throw new RequestException(
					"Multiple datasets were marked as primary, only one dataset may be marked as primary.");
		}
		// TODO do you need to merge the xml documents?
		XmlObject xmlObject = DatasetManagerUtils.parseInputStream(xmlFiles[primaryXmlIndex]);
		return generator.getUpdatedXmlAsString(fileInformation, program, datasetsList, xmlObject);
	}

	/**
	 * if no file info is available, match only on the xml name and return
	 * immediately. populate the dataset list by sending the multipart files
	 * through the reader. transform multipart files to xml objects first thing
	 * so you don't have to read input stream again
	 */
	private String updateSingleFile(Ddi25OutputGenerator generator, Program program, List<XmlObject> xmlObjects) {
		if (xmlObjects.isEmpty()) {
			throw new UpdaterException("No XML objects found in the upater manager");
		}
		List<DataSet> datasetsList = readDataSets(xmlObjects.toArray(new XmlObject[0]));
		/*
		 * only using the first xmlObject as a basis for the new xml document
		 */
		return generator.getUpdatedXmlAsString(null, program, datasetsList, xmlObjects.get(0));
	}

	/**
	 * The processor needs a script name to run correctly. Ideally this would be
	 * provided in the input information, but if not, use the provided xml name,
	 * then if that fails, use the dataset's own file Dscr Name
	 * 
	 * @param info
	 * @param ds
	 * @return
	 */
	private String getScriptNameOrEquivalent(InputInformation info, DataSet ds) {
		String scriptName = info.getScriptName();
		String xmlName = info.getXmlName();
		if (scriptName == null || scriptName.isEmpty()) {
			// set the script name to the name of the one at the correct xml
			// index so it can be processed normally.
			if (xmlName != null && !xmlName.isEmpty()) {
				scriptName = xmlName;
			} else {
				// assign it from the ds at the index.
				// would this ever happen?
				// scriptName = ds.getFileDscrName();
				scriptName = ds.getName();

			}
		}
		return scriptName;
	}

	/**
	 * Read the xml object into the correct reader implementation (EML or DDI)
	 * 
	 * @param xml
	 * @return list of parsed dataset objects
	 */
	private List<DataSet> readDataSets(XmlObject... xmlObjects) {
		// should you pass in the list in case any already exist?
		List<DataSet> datasets = new ArrayList<>();
		for (XmlObject xml : xmlObjects) {
			if (xml != null) {
				// add them all to the list
				datasets.addAll(DatasetManagerUtils.getReaderImpl(xml).getDataSets(xml));
			}
		}
		return datasets;
	}

	/**
	 * take a list of datasets but only return the one that matches the xml file
	 * name. then set the script name on the found dataset. if none is found to
	 * match, it will return null and other matching strategies will be used.
	 * 
	 * @param obj
	 * @param xmlName
	 * @param position
	 * @return dataset matching the provided criteria
	 */
	private DataSet readDataSets(XmlObject obj, String xmlName, Integer position) {
		DataSet ds = null;
		if (obj != null) {
			Reader reader = DatasetManagerUtils.getReaderImpl(obj);
			// this is one-based.
			int xmlIndex = 1;
			for (DataSet readDs : reader.getDataSets(obj)) {
				// if the dataset's name matches the xml name given, return that
				// one.
				if (readDs.getName().equals(xmlName) || readDs.getDatasetId().equals(xmlName)) {
					ds = readDs;
				} else {
					// match the dataset on the index, if one is given
					if (position != null) {
						if (xmlIndex == position) {
							ds = readDs;
						}
					} else {
						// FIXME return the first datset found?
						return readDs;
					}
				}
				xmlIndex++;
			}
		}
		return ds;
	}

	public String getPseudocodeHost() {
		log.debug("PSEUDOCODE HOST IS ["+pseudocodeHost+"] in the updater manager getPseudocodeHost method");
		return pseudocodeHost;
	}

	public ValidationResult validateSdtl(Program program) {
		ValidationResult result = ValidationUtility.validate(program);
		return result;
	}

}

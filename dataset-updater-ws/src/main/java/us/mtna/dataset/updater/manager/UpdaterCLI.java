package us.mtna.dataset.updater.manager;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.c2metadata.sdtl.pojo.Program;

import com.fasterxml.jackson.databind.ObjectMapper;

import us.mtna.data.transform.wrapper.sdtl.ValidationResult;
import us.mtna.dataset.updater.InputInformation;
import us.mtna.dataset.updater.ValidationUtility;

/**
 * Use the dataset updater as a command line service.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class UpdaterCLI {

	private static String updaterVersion = "CLI-1.1.1";
	//build jar with maven package
	// run as "java UpdaterCLI --xml test.xml"
	public static void main(String[] args) throws ParseException, IOException {

		UpdaterManager updaterManager = new UpdaterManager("", "", "CLI-1.0.0");
		ObjectMapper mapper = new ObjectMapper();

		// build up the options
		Option help = Option.builder("help").desc("Access this help menu").build();
		Option version = Option.builder("version").desc("Gets the version of this CLI").build();
		Option xml = Option.builder("xml").hasArgs().desc("Path(s) to one or more xml files to be updated")
				.argName("Path(s)").build();
		Option sdtl = Option.builder("sdtl").hasArg().argName("Path").desc("Path to SDTL JSON file")
				.build();
		Option validate = Option.builder("validate")
				.desc("Validate sdtl file - the updater process will not be run.").build();
		Option output = Option.builder("output").hasArg().argName("Output").desc("Name of the file to write the resulting output to. If this arg is left off, output will be printed to the console.")
				.build();
		// add the options
		Options options = new Options();
		options.addOption(xml);
		options.addOption(help);
		options.addOption(version);
		options.addOption(sdtl);
		options.addOption(validate);
		options.addOption(output);


		HelpFormatter formatter = new HelpFormatter();

		CommandLineParser parser = new DefaultParser();
		CommandLine cmd = parser.parse(options, args);

		File xmlFile = null;
		InputInformation[] info = null;
		File programFile = null;
		XmlObject obj = null;

		if (cmd.hasOption("help") || cmd.hasOption("h")) {
			formatter.printHelp("java -jar ds-updater-ws.jar <options>", options);
			return;
		}
		
		if (cmd.hasOption("version") || cmd.hasOption("vs")) {
			System.out.println("Version: " + updaterVersion);
			return;
		}

		if (cmd.hasOption("xml") || cmd.hasOption("x")) {
			String inputFile = cmd.getOptionValue("xml");
			if (new File(inputFile).exists()) {
				xmlFile = new File(inputFile);
				try {
					obj = XmlObject.Factory.parse(xmlFile);
				} catch (XmlException e) {
					throw new RuntimeException(e);
				}
			}
		}
		//TODO this says it will only check for the short name of an option, 
		if (cmd.hasOption("sdtl")) {
			programFile = new File(cmd.getOptionValue("sdtl"));
		}

		if (cmd.hasOption("validate")) {
			if (programFile.exists()) {

				Program p = mapper.readValue(programFile, Program.class);
				ValidationResult result = ValidationUtility.validate(p);
				if (result.isValid()) {
					System.out.println("Document is valid");
					return;
				} else {
					System.out.println("Errors found in SDTL document:");
					for (String s : result.getMessages()) {
						System.out.println(s);
					}
					return;
				}
			} else {
				System.out.println("Could not find SDTL file");
				return;
			}
		}

		if (cmd.hasOption("fileInfo")) {
			File fileInfoFile = new File(cmd.getOptionValue("fileInfo"));
			info = mapper.readValue(fileInfoFile, InputInformation[].class);
		}
		
		
		if (programFile != null && obj != null) {
			String updated = updaterManager.uploadInuptInformation(info, programFile, obj);
			if (cmd.hasOption("output")) {
				String outputFileName = cmd.getOptionValue("output");

				//write it to a file in this directory	
				List<String> lines = Arrays.asList(updated);
				Path file = Paths.get(outputFileName);
				Files.write(file, lines, StandardCharsets.UTF_8);
			}else{
				System.out.println(updated);
			}
			
			
		}else{
			System.out.println("json and/or xml were null");
		}

	}

}

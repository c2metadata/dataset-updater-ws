/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.dataset.updater.ws;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.c2metadata.sdtl.pojo.Program;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.fasterxml.jackson.databind.JsonMappingException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Info;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import us.mtna.data.transform.wrapper.sdtl.ValidationResult;
import us.mtna.dataset.updater.InputInformation;
import us.mtna.dataset.updater.exception.C2MError;
import us.mtna.dataset.updater.exception.InvalidSdtlException;
import us.mtna.dataset.updater.exception.TransformationException;
import us.mtna.dataset.updater.exception.UpdaterException;
import us.mtna.dataset.updater.manager.UpdaterManager;
import us.mtna.reader.exceptions.BaseC2MException;
import us.mtna.reader.exceptions.ReaderException;
import us.mtna.reader.exceptions.RequestException;
import us.mtna.reader.exceptions.XmlValidationException;

@SwaggerDefinition(tags = {
		@Tag(name = "update", description = "Upload datasets to be read") }, info = @Info(title = "Dataset Updater API", description = "", version = "0.0.0"))
@RestController
@RequestMapping("/")
@Api
public class UpdaterController {

	private Log log = LogFactory.getLog(this.getClass());
	private UpdaterManager updaterManager;
	/**
	 * update for every docker release. To be displayed on the UI for debugging
	 */
	private static String releaseDate = "6/17/2021";

	public UpdaterController(UpdaterManager manager) {
		this.updaterManager = manager;
	}

	@ApiOperation(value = "Get the date of the latest release for this version of the dataset updater API currently in use.", tags = "Manage")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Version retrieved successfully.") })
	@RequestMapping(value = "lastRelease", method = RequestMethod.GET)
	public String getLatestRelease() {
		return releaseDate;
	}

	@ApiOperation(value = "Get pseudocode host", tags = "Manage")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Version retrieved successfully.") })
	@RequestMapping(value = "pseudocodeHost", method = RequestMethod.GET)
	public String getPseudocodeHost() {
		return updaterManager.getPseudocodeHost();
	}

	@ApiOperation(value = "Get a blank InputInfo object to fill out and send back in the /upload method", tags = "Manage")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "New mapping object successfully returned") })
	@RequestMapping(value = "inputInfo", method = RequestMethod.GET)
	public InputInformation getBlankInputInfo() {
		return new InputInformation();
	}

	@ApiOperation(value = "Validate SDTL", tags = "Manage")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Validation Completed") })
	@RequestMapping(value = "validate", method = RequestMethod.POST)
	public ValidationResult validateSdtl(
			@ApiParam(name = "sdtl", value = "SDTL JSON containing the transformed commands that will be used to update the XML file", required = true) @RequestPart(value = "sdtl", required = true) Program program) {
		return updaterManager.validateSdtl(program);
	}

	@ApiOperation(tags = "End to End", value = "Upload one or more XML files, their corresponding Input Information objects, and the SDTL Program object and get back the updated dataset as a string")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = String.class, message = "Updated XML document as a string"),
			@ApiResponse(code = 512, response = TransformationException.class, message = "A problem was found with the SDTL Transformation File."),
			@ApiResponse(code = 500, response = UpdaterException.class, message = "Server error: there was a problem with the updater."),
			@ApiResponse(code = 400, response = RequestException.class, message = "Bad Request") })
	@RequestMapping(value = "update/xml", method = RequestMethod.POST, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE,
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public void useInputInformation(@RequestHeader HttpHeaders headers,HttpServletResponse response,
			@ApiParam(name = "config", value = "Input Information configuration object to describe one dataset in the request. Need one input information per file passed in.", required = false) @RequestPart(value = "config", required = false) InputInformation[] fileInformation,
			@ApiParam(name = "sdtl", value = "SDTL JSON containing the transformed commands that will be used to update the XML file", required = true) @RequestPart(value = "sdtl", required = true) Program program,
			@ApiParam(name = "datasets", value = "An array of files containing the datasets that will be updated", required = true) @RequestPart(value = "datasets", required = true) MultipartFile... datasets) {
		String xml = updaterManager.uploadInuptInformation(fileInformation, program, datasets);
		response.setContentType("text/xml; charset=utf-8");
		try {
			response.getWriter().write(xml);
		} catch (IOException e) {
			throw new RequestException(e);
		}
	}

	@Bean(name = "multipartResolver")
	public CommonsMultipartResolver multipartResolver() {
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		return multipartResolver;
	}

	@ExceptionHandler(TransformationException.class)
	public ResponseEntity<C2MError> handleTransformationException(HttpServletRequest req,
			TransformationException exception) {
		HttpStatus status = HttpStatus.BAD_REQUEST;

		if (exception instanceof InvalidSdtlException) {
			InvalidSdtlException e = (InvalidSdtlException) exception;
			return getErrorResponse(req, status, exception, e.getMessages());
		}
		return getErrorResponse(req, status, exception);
	}

	@ExceptionHandler(RequestException.class)
	public ResponseEntity<C2MError> handleRequestException(HttpServletRequest req, RequestException exception) {
		HttpStatus status = HttpStatus.BAD_REQUEST;
		if (exception instanceof XmlValidationException) {
			XmlValidationException e = (XmlValidationException) exception;
			return getErrorResponse(req, status, exception, e.getErrorMessages());
		}
		return getErrorResponse(req, status, exception);
	}
	
	
	@ExceptionHandler(ReaderException.class)
	public ResponseEntity<C2MError> handleReaderException(HttpServletRequest req, ReaderException exception) {
		HttpStatus status = HttpStatus.BAD_REQUEST;
//		if (exception instanceof XmlValidationException) {
//			XmlValidationException e = (XmlValidationException) exception;
//			return getErrorResponse(req, status, exception, e.getErrorMessages());
//		}
		return getErrorResponse(req, status, exception);
	}

	@ExceptionHandler(BaseC2MException.class)
	public ResponseEntity<C2MError> handleBaseException(HttpServletRequest req, BaseC2MException exception) {
		HttpStatus status = HttpStatus.BAD_REQUEST;
		return getErrorResponse(req, status, exception);
	}

	@ExceptionHandler(UpdaterException.class)
	public ResponseEntity<C2MError> handleUpdaterException(HttpServletRequest req, UpdaterException exception) {
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		return getErrorResponse(req, status, exception);
	}
	@ExceptionHandler(JsonMappingException.class)
	public ResponseEntity<C2MError> handleJsonMappingException(HttpServletRequest req, JsonMappingException exception) {
		HttpStatus status = HttpStatus.BAD_REQUEST;
		System.out.println(req.getRequestURI());
		if(exception instanceof JsonMappingException){
			List<String> messages = new ArrayList<>();
			messages.add("Invalid SDTL detected.");
			messages.add(exception.getMessage());
			InvalidSdtlException sdtlException = new InvalidSdtlException(messages);
			
			return getErrorResponse(req, HttpStatus.BAD_REQUEST, sdtlException); 
			
		}
//		if(req.getRequestURI())
		//eventually give different level of errors
		//check to see if validate was called - if so return a 200 
		//have the first message that the sdtl wasn't valid, then output the message from the exception
//		req.
		return getErrorResponse(req, status, exception);
	}
	private ResponseEntity<C2MError> getErrorResponse(HttpServletRequest req, HttpStatus status, Exception exception) {
		C2MError message = new C2MError(exception);
		message.setCode(status.value());
		if(InvalidSdtlException.class.isAssignableFrom(exception.getClass())){
			InvalidSdtlException se = (InvalidSdtlException) exception;
			message.setErrors(se.getMessages());

		}

		return new ResponseEntity<>(message, status);
	}

	private ResponseEntity<C2MError> getErrorResponse(HttpServletRequest req, HttpStatus status, Exception exception,
			List<String> messages) {
		C2MError message = new C2MError(messages);
		message.setCode(status.value());
		return new ResponseEntity<>(message, status);
	}

}

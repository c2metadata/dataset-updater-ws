### Quick Reference
#### Dataset Updater endpoints:
This API is configured to be used with the C2M end to end UI. Below is a short summary of the available endpoints, more information can be found in the postman documentation below.

* GET **/inputInfo** - get the configuration object to pass in. 
* POST **/update/xml** - pass in an SDTL file (as a body RequestPart), an array of InputInformation objects (one for each file, also a body RequestPart), and an array of xml files (body RequestPart)
* GET **/lastRelease** - gets the date of the latest API release 
* GET **/pseudocodeHost** - gets the currently configured host for the pseudocode service
* POST **/validate** - pass in an SDTL file (as a body RequestPart), and it will be validated against the current models in use for the updater. 
 

#### Variable/Dataset Information endpoints:
These endpoints were created to be used by the stats parsers to be able to pull variable and dataset information from an XML document. Example calls and responses can be found in the postman documentation. 
All of these calls take an xml document as a body (formdata) parameter named "file".

* POST **/info/variables/names** - get a list of variable names found in the provided XML document. (This is probably the most useful one for stats parsers, the others just provide extra utility) )
* POST **/info/variables/ids** - get a list of variable ids found in the provided XML document.
* POST **/info/variables** - get a list of variable objects found in the provided XML document.
* POST **/info/datasets** - get a list of dataset objects found in the provided XML document.


[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/a74ef5353e4cb3ad7ba7)

## Use the docker image:
```
docker pull registry.gitlab.com/c2metadata/dataset-updater-ws:latest
docker run -d -p 8080:8080 registry.gitlab.com/c2metadata/dataset-updater-ws
```

## Pseudocode Service Integration:
When running the updater process, the service will attempt to get the pseudocode translation for each transform from the [pseudocodeservice-web project](https://gitlab.com/c2metadata/pseudocodeservice-web) using the /command/text endpoint. The default host value is http://52.90.103.106:8092, but this can be configured via a docker environment variable.

To set the host to a different value, pass it in to the docker run command with the param "-e PSEUDOCODE_HOST=yourHostAndPort". An example docker run command to change the host to localhost:8082 would be: 
```
 docker run -p 8080:8080 -d -e PSEUDOCODE_HOST=http://localhost:8082 registry.gitlab.com/c2metadata/dataset-updater-ws:latest 
```

Note that you should inclue the protocol, host, and port. You can also check the currently configured host by calling /pseudocodeHost.

## Documentation:

[Online postman documentation - includes variable/dataset information endpoints](https://documenter.getpostman.com/view/2220438/SW14WHVi?version=latest#c5d92df6-926a-4528-b760-d86d34380831)


## Use via command line:
There is now a command line version of the Dataset Updater. You will need the jar from the folder [here](https://gitlab.com/c2metadata/dataset-updater-ws/-/tree/master/dataset-updater-ws/cli) to run it. 

* Update an xml file:  
`java -jar dataset-updater-ws-jar-with-dependencies.jar -xml /path/to/file.xml -sdtl /path/to/sdtl.json`

* Update using more than one xml file (merge, append, etc.):  
`java -jar dataset-updater-ws-jar-with-dependencies.jar -xml /path/to/file1.xml /path/to/file2.xml -sdtl /path/to/sdtl.json`
    * example using the append use case in postmanFiles/multiFile/append: `java -jar dataset-updater-ws-jar-with-dependencies.jar -xml 621ddi.xml 626ddi.xml -sdtl append.json -output yourOutput.xml`. 



* Validate SDTL (This will validate the SDTL against the models we are currently using in the Updater. We try to stay on top of any updates to the SDTL model, which you can check out [here](http://c2metadata.gitlab.io/sdtl-docs/master/composite-types/TransformBase/)):  
`java -jar dataset-updater-ws-jar-with-dependencies.jar -validate -sdtl /path/to/sdtl.json`

* By default, resulting output will be printed to the console, but to write to a file, include `-output yourfilename.xml`. This file will be written to the current directory.

Please let me know of any problems, questions, or feature ideas. 


## ...or via cURL
To use the updater with cURL, use the following format:  
`curl -X POST -H "Content-Type: multipart/form-data" -F "sdtl=@/path/to/sdtl.json;type=application/json" -F "datasets=@/path/to/ddi.xml;type=application/json" http://52.90.103.106:8090/dataset-updater-ws/update/xml`  


## Link to exceptions document:

[exceptions](https://gitlab.com/c2metadata/dataset-updater-ws/blob/master/exceptions.md)


## Updates:
### Upcoming release: June 2020
* [ ] BOM reader  
* [ ] Pseudocode generator integration  
* [ ] integrate all parsers  



### March 2020 - docker tag 1.1.8
* Update default url for pseudocode generator to http://52.90.103.106:8092 - this is still configurable via the docker image, this is just what it will default to now with no configuration
* updated to handle parser input with uppercased json keys
* model updates
* added command line tool - see section of readme above. 
* updating merge processes to be in alignment with the merge descriptions 
* added endpoint for validating sdtl (/validate)

### 2/11/2020 - docker tag 1.1.3
* updates to january sdtl-cogs pull request
* fixed bug where duplicate file names would cause exception

### 11/4/2019
* Added variable/dataset information endpoints
* Updated updater to use latest model changes.
* Updated documentation 

### 7/31/2019
* Updated error handling
* Fixed issue with Collapse SDTL command throwing an error.
* clearer versioning of this and child projects.
* removing and deprecating unused methods in the DDI25OutputGenerator.
* Integration of pseudocode generator (see "Pseudocode Service Integration" section above)

### 6/4/2019
* Updates to support uploads without InputInformation configuration.
* new configuration options: pass in title, author, id to the InputInformation to be used in creating the file-level metadata. 
* updated openmetadata-model to include consumesDataframe and producesDataframe.
* added xml validation and an XmlValidationException 
* added "testing" docker tag - this is the latest version for us to test on, the more stable version will stay at the "latest" tag.

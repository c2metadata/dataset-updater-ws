* BaseException - base for all of our exceptions
    * RequestException - exception was caused by a bad request by the user (error code 400)
        * MissingFilesException 
        * UnknownDatasetException -  refers to a dataset file that cannot be determined by the updater -this is thrown when a primary dataset is not specified either specifically by the user or with a load command in the script
        * MisisngSourceVariableException - variable is null/doesn’t exist so we can’t update it
        * XmlValidationException - one or more of the XML files passed in is invalid. This exception holds both a list of string messages and a list of XmlError objects that hold more information about the errors found. You can call exception.getErrorMessages() and exception.getErrors() respectively to access these.
    * UpdaterException - base for server exceptions caused by an Updater malfunction. (error codes 500-510)
        * ReaderException - error occurred while reading the file
    * Transformation exception - errors for other groups (error codes 511-520)
        * InvalidSdtlException - sdtl does not align with the model/validation rules. Has “language” property on it so that correct group can be notified.


### Things that could result in a bad request:
* marking two datasets as primary (marking zero as primary is ok.)
* number of input information objects != number of files passed in
* invalid XML
